# French translations for PACKAGE package.
# Copyright (C) 2017 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-11 17:08+0700\n"
"PO-Revision-Date: 2023-07-02 18:50+0000\n"
"Last-Translator: spnux <stephane.petrus@posteo.net>\n"
"Language-Team: French <https://hosted.weblate.org/projects/ayatana-"
"indicators/display-applet/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.0-dev\n"

#: src/usb-snap.cpp:109
#, c-format
msgid "The computer's RSA key fingerprint is: %s"
msgstr "La clé de l'empreinte de l'ordinateur du RSA est %s"

#: src/usb-snap.cpp:114
msgid "Allow"
msgstr "Autoriser"

#: src/usb-snap.cpp:116
msgid "Don't Allow"
msgstr "Ne pas autoriser"

#: src/usb-snap.cpp:125
msgid "Allow USB Debugging?"
msgstr "Autoriser le débogage USB ?"

#~ msgid "Lock rotation"
#~ msgstr "Bloquer la rotation"

#, fuzzy
#~ msgid "Orientation lock"
#~ msgstr "Verrouiller la rotation"

#~ msgid "Locks orientation to a specific value."
#~ msgstr "Bloquer la rotation à une valeur spécifique."

#~ msgid "Color temperature"
#~ msgstr "Température de la couleur"

#~ msgid ""
#~ "Sets the color temperature of your screen. The lower the value, the "
#~ "higher the intensity of the redness effect applied to the display."
#~ msgstr ""
#~ "Définit la température de la couleur de votre écran. Plus la valeur est "
#~ "basse, plus l'effet de rougeur appliqué à l'écran est intense."

#~ msgid "Color temperature profile"
#~ msgstr "Profil de température de la couleur"

#~ msgid "The current color temperature profile being used by the indicator."
#~ msgstr ""
#~ "Le profil de température de la couleur actuellement utilisé par "
#~ "l'indicateur."

#~ msgid "Screen brightness"
#~ msgstr "Luminosité de l'écran"

#~ msgid "Stores the current brightness value of your screen."
#~ msgstr "Enregistre la valeur de la luminosité actuelle de votre écran."

#, fuzzy
#~ msgid "The current theme profile being used by the indicator."
#~ msgstr ""
#~ "Le profil de température de la couleur actuellement utilisé par "
#~ "l'indicateur."

#~ msgid "Manual"
#~ msgstr "Manuel"

#~ msgid "Rotation Lock"
#~ msgstr "Verrouiller la rotation"

#~ msgid "Brightness"
#~ msgstr "Luminosité"

#~ msgid "Display settings…"
#~ msgstr "Paramètres d'affichage…"

#~ msgid "Rotation"
#~ msgstr "Rotation"

#~ msgid "Display"
#~ msgstr "Affichage"

#~ msgid "Display settings and features"
#~ msgstr "Réglages et caractéristiques de l'affichage"

#~ msgid "Color temperature profiles"
#~ msgstr "Profils de température de la couleur"
