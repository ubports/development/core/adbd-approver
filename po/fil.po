# Filipino translations for PACKAGE package.
# Copyright (C) 2017 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-11 17:08+0700\n"
"PO-Revision-Date: 2017-11-28 08:50+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: fil\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/usb-snap.cpp:109
#, c-format
msgid "The computer's RSA key fingerprint is: %s"
msgstr ""

#: src/usb-snap.cpp:114
msgid "Allow"
msgstr ""

#: src/usb-snap.cpp:116
msgid "Don't Allow"
msgstr ""

#: src/usb-snap.cpp:125
msgid "Allow USB Debugging?"
msgstr ""
