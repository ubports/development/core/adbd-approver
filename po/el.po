# Greek translations for PACKAGE package.
# Copyright (C) 2017 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-11 17:08+0700\n"
"PO-Revision-Date: 2019-05-21 12:43+0000\n"
"Last-Translator: THANOS SIOURDAKIS <siourdakisthanos@gmail.com>\n"
"Language-Team: Greek <https://hosted.weblate.org/projects/ayatana-indicators/"
"display-applet/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.7-dev\n"

#: src/usb-snap.cpp:109
#, c-format
msgid "The computer's RSA key fingerprint is: %s"
msgstr "Το αποτύπωμα κλειδιού RSA του υπολογιστή είναι: %s"

#: src/usb-snap.cpp:114
msgid "Allow"
msgstr "Επέτρεψε"

#: src/usb-snap.cpp:116
msgid "Don't Allow"
msgstr "Μην Επιτρέψεις"

#: src/usb-snap.cpp:125
msgid "Allow USB Debugging?"
msgstr "Να επιτραπεί ο εντοπισμός σφαλμάτων USB;"

#, fuzzy
#~ msgid "Lock rotation"
#~ msgstr "Περιστροφή"

#, fuzzy
#~ msgid "Orientation lock"
#~ msgstr "Κλείδωμα Περιστροφής"

#~ msgid "Rotation Lock"
#~ msgstr "Κλείδωμα Περιστροφής"

#~ msgid "Rotation"
#~ msgstr "Περιστροφή"
