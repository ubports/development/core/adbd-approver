/*
 * Copyright 2014 Canonical Ltd.
 * Copyright 2022-2023 Robert Tari
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *   Charles Kerr <charles.kerr@canonical.com>
 *   Robert Tari <robert@tari.in>
 */

#include <sys/stat.h>
#include <errno.h>

#include <src/greeter.h>
#include <src/usb-manager.h>
#include <src/usb-monitor.h>

#include <glib/gi18n.h> // bindtextdomain()
#include <gio/gio.h>
#include <locale.h>

int
main(int /*argc*/, char** /*argv*/)
{
    // boilerplate i18n
    setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    auto loop = g_main_loop_new(nullptr, false);

    static constexpr char const * ADB_SOCKET_PATH {"/dev/socket/adbd"};
    static constexpr char const * PUBLIC_KEYS_FILENAME {"/data/misc/adb/adb_keys"};
    auto usb_monitor = std::make_shared<GUDevUsbMonitor>();
    auto greeter = std::make_shared<Greeter>();
    UsbManager usb_manager {ADB_SOCKET_PATH, PUBLIC_KEYS_FILENAME, usb_monitor, greeter};

    // let's go!
    g_main_loop_run(loop);

    // cleanup
    g_main_loop_unref(loop);
    return 0;
}
